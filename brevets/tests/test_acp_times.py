from nose.tools import *
import acp_times



def test_200km_brev():
    """Test a standard 200km brevet w/ control points beyond brevet length"""
    assert acp_times.open_time(0, 200, '2021-01-01T00:00+00:00') == '2021-01-01T00:00:00+00:00'
    assert acp_times.open_time(60, 200, '2021-01-01T00:00+00:00') == '2021-01-01T01:46:00+00:00'
    assert acp_times.open_time(120, 200, '2021-01-01T00:00+00:00') == '2021-01-01T03:32:00+00:00'
    assert acp_times.open_time(175, 200, '2021-01-01T00:00+00:00') == '2021-01-01T05:09:00+00:00'
    assert acp_times.open_time(200, 200, '2021-01-01T00:00+00:00') == '2021-01-01T05:53:00+00:00'
    assert acp_times.open_time(220, 200, '2021-01-01T00:00+00:00') == '2021-01-01T05:53:00+00:00'

    assert acp_times.close_time(0, 200, '2021-01-01T00:00+00:00') == '2021-01-01T01:00:00+00:00'
    assert acp_times.close_time(60, 200, '2021-01-01T00:00+00:00') == '2021-01-01T04:00:00+00:00'
    assert acp_times.close_time(120, 200, '2021-01-01T00:00+00:00') == '2021-01-01T08:00:00+00:00'
    assert acp_times.close_time(175, 200, '2021-01-01T00:00+00:00') == '2021-01-01T11:40:00+00:00'
    assert acp_times.close_time(200, 200, '2021-01-01T00:00+00:00') == '2021-01-01T13:30:00+00:00'
    assert acp_times.close_time(220, 200, '2021-01-01T00:00+00:00') == '2021-01-01T13:30:00+00:00'



def test_300km_brev():
    """Test a standard 300km brevet w/ control points beyond brevet length"""
    assert acp_times.open_time(0, 300, '2021-01-01T00:00+00:00') == '2021-01-01T00:00:00+00:00'
    assert acp_times.open_time(150, 300, '2021-01-01T00:00+00:00') == '2021-01-01T04:25:00+00:00'
    assert acp_times.open_time(250, 300, '2021-01-01T00:00+00:00') == '2021-01-01T07:27:00+00:00'
    assert acp_times.open_time(300, 300, '2021-01-01T00:00+00:00') == '2021-01-01T09:00:00+00:00'
    assert acp_times.open_time(330, 300, '2021-01-01T00:00+00:00') == '2021-01-01T09:00:00+00:00'

    assert acp_times.close_time(0, 300, '2021-01-01T00:00+00:00') == '2021-01-01T01:00:00+00:00'
    assert acp_times.close_time(150, 300, '2021-01-01T00:00+00:00') == '2021-01-01T10:00:00+00:00'
    assert acp_times.close_time(250, 300, '2021-01-01T00:00+00:00') == '2021-01-01T16:40:00+00:00'
    assert acp_times.close_time(300, 300, '2021-01-01T00:00+00:00') == '2021-01-01T20:00:00+00:00'
    assert acp_times.close_time(330, 300, '2021-01-01T00:00+00:00') == '2021-01-01T20:00:00+00:00'



def test_400km_brev():
    """Test a standard 400km brevet w/ control points beyond brevet length"""
    assert acp_times.open_time(0, 400, '2021-01-01T00:00+00:00') == '2021-01-01T00:00:00+00:00'
    assert acp_times.open_time(100, 400, '2021-01-01T00:00+00:00') == '2021-01-01T02:56:00+00:00'
    assert acp_times.open_time(200, 400, '2021-01-01T00:00+00:00') == '2021-01-01T05:53:00+00:00'
    assert acp_times.open_time(300, 400, '2021-01-01T00:00+00:00') == '2021-01-01T09:00:00+00:00'
    assert acp_times.open_time(400, 400, '2021-01-01T00:00+00:00') == '2021-01-01T12:08:00+00:00'
    assert acp_times.open_time(440, 400, '2021-01-01T00:00+00:00') == '2021-01-01T12:08:00+00:00'

    assert acp_times.close_time(0, 400, '2021-01-01T00:00+00:00') == '2021-01-01T01:00:00+00:00'
    assert acp_times.close_time(100, 400, '2021-01-01T00:00+00:00') == '2021-01-01T06:40:00+00:00'
    assert acp_times.close_time(200, 400, '2021-01-01T00:00+00:00') == '2021-01-01T13:20:00+00:00'
    assert acp_times.close_time(300, 400, '2021-01-01T00:00+00:00') == '2021-01-01T20:00:00+00:00'
    assert acp_times.close_time(400, 400, '2021-01-01T00:00+00:00') == '2021-01-02T03:00:00+00:00'
    assert acp_times.close_time(440, 400, '2021-01-01T00:00+00:00') == '2021-01-02T03:00:00+00:00'



def test_600km_brev():
    """Test a standard 600km brevet w/ control points beyond brevet length"""
    assert acp_times.open_time(0, 600, '2021-01-01T00:00+00:00') == '2021-01-01T00:00:00+00:00'
    assert acp_times.open_time(100, 600, '2021-01-01T00:00+00:00') == '2021-01-01T02:56:00+00:00'
    assert acp_times.open_time(200, 600, '2021-01-01T00:00+00:00') == '2021-01-01T05:53:00+00:00'
    assert acp_times.open_time(300, 600, '2021-01-01T00:00+00:00') == '2021-01-01T09:00:00+00:00'
    assert acp_times.open_time(400, 600, '2021-01-01T00:00+00:00') == '2021-01-01T12:08:00+00:00'
    assert acp_times.open_time(500, 600, '2021-01-01T00:00+00:00') == '2021-01-01T15:28:00+00:00'
    assert acp_times.open_time(600, 600, '2021-01-01T00:00+00:00') == '2021-01-01T18:48:00+00:00'
    assert acp_times.open_time(660, 600, '2021-01-01T00:00+00:00') == '2021-01-01T18:48:00+00:00'

    assert acp_times.close_time(0, 600, '2021-01-01T00:00+00:00') == '2021-01-01T01:00:00+00:00'
    assert acp_times.close_time(100, 600, '2021-01-01T00:00+00:00') == '2021-01-01T06:40:00+00:00'
    assert acp_times.close_time(200, 600, '2021-01-01T00:00+00:00') == '2021-01-01T13:20:00+00:00'
    assert acp_times.close_time(300, 600, '2021-01-01T00:00+00:00') == '2021-01-01T20:00:00+00:00'
    assert acp_times.close_time(400, 600, '2021-01-01T00:00+00:00') == '2021-01-02T02:40:00+00:00'
    assert acp_times.close_time(500, 600, '2021-01-01T00:00+00:00') == '2021-01-02T09:20:00+00:00'
    assert acp_times.close_time(600, 600, '2021-01-01T00:00+00:00') == '2021-01-02T16:00:00+00:00'
    assert acp_times.close_time(660, 600, '2021-01-01T00:00+00:00') == '2021-01-02T16:00:00+00:00'



def test_1000km_brev():
    """Test a standard 1000km brevet w/ control points beyond brevet length"""
    assert acp_times.open_time(0, 1000, '2021-01-01T00:00+00:00') == '2021-01-01T00:00:00+00:00'
    assert acp_times.open_time(100, 1000, '2021-01-01T00:00+00:00') == '2021-01-01T02:56:00+00:00'
    assert acp_times.open_time(300, 1000, '2021-01-01T00:00+00:00') == '2021-01-01T09:00:00+00:00'
    assert acp_times.open_time(500, 1000, '2021-01-01T00:00+00:00') == '2021-01-01T15:28:00+00:00'
    assert acp_times.open_time(700, 1000, '2021-01-01T00:00+00:00') == '2021-01-01T22:22:00+00:00'
    assert acp_times.open_time(900, 1000, '2021-01-01T00:00+00:00') == '2021-01-02T05:31:00+00:00'
    assert acp_times.open_time(1000, 1000, '2021-01-01T00:00+00:00') == '2021-01-02T09:05:00+00:00'
    assert acp_times.open_time(1100, 1000, '2021-01-01T00:00+00:00') == '2021-01-02T09:05:00+00:00'


    assert acp_times.close_time(0, 1000, '2021-01-01T00:00+00:00') == '2021-01-01T01:00:00+00:00'
    assert acp_times.close_time(100, 1000, '2021-01-01T00:00+00:00') == '2021-01-01T06:40:00+00:00'
    assert acp_times.close_time(300, 1000, '2021-01-01T00:00+00:00') == '2021-01-01T20:00:00+00:00'
    assert acp_times.close_time(500, 1000, '2021-01-01T00:00+00:00') == '2021-01-02T09:20:00+00:00'
    assert acp_times.close_time(700, 1000, '2021-01-01T00:00+00:00') == '2021-01-03T00:45:00+00:00'
    assert acp_times.close_time(900, 1000, '2021-01-01T00:00+00:00') == '2021-01-03T18:15:00+00:00'    
    assert acp_times.close_time(1000, 1000, '2021-01-01T00:00+00:00') == '2021-01-04T03:00:00+00:00'
    assert acp_times.close_time(1100, 1000, '2021-01-01T00:00+00:00') == '2021-01-04T03:00:00+00:00'



def test_too_fars():
    """Test Open and close times of control points that are > 120% of brevet length"""
    assert acp_times.open_time(241, 200, '2021-01-01T00:00+00:00') == "Error: Control point is too far out"
    assert acp_times.open_time(361, 300, '2021-01-01T00:00+00:00') == "Error: Control point is too far out"
    assert acp_times.open_time(481, 400, '2021-01-01T00:00+00:00') == "Error: Control point is too far out"
    assert acp_times.open_time(721, 600, '2021-01-01T00:00+00:00') == "Error: Control point is too far out"
    assert acp_times.open_time(1201, 1000, '2021-01-01T00:00+00:00') == "Error: Control point is too far out"

    assert acp_times.close_time(241, 200, '2021-01-01T00:00+00:00') == "Error: Control point is too far out"
    assert acp_times.close_time(361, 300, '2021-01-01T00:00+00:00') == "Error: Control point is too far out"
    assert acp_times.close_time(481, 400, '2021-01-01T00:00+00:00') == "Error: Control point is too far out"
    assert acp_times.close_time(721, 600, '2021-01-01T00:00+00:00') == "Error: Control point is too far out"
    assert acp_times.close_time(1201, 1000, '2021-01-01T00:00+00:00') == "Error: Control point is too far out"




