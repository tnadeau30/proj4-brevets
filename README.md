## Project #4: Brevet time calculator with Ajax

Author: Timothy Nadeau (tnadeau@uoregon.edu)

Class: CIS 322 (Fall 2021)

Professor: Ram Durairajan


## Description

This repo contains what is needed to construct an AJAX brevet control time calculator.

When compiled as an image, and then run the container, this program will allow you to access a site with a table and some other adjustable variables such as brevet distance and start time. When inputting a number (milage/km-age) in the miles/km cell, given the current start time, and brevet distance, the calculator will return the open and close times for the given control point at 'X' km.


Notes:
- This calculator follows the behaviour calculation-wise of the [ACP Brevet Control Times Calculator](https://rusa.org/pages/acp-brevet-control-times-calculator), not including the French Oddities.
- This calculator also follows [RUSA rules](https://rusa.org/pages/rulesForRiders)


## Construction

To construct this calculator, first download the repo and goto the 'brevets' directory via your terminal/command prompt

```~/brevets/     ```

Once there, build the docker image:

```docker build -t p4-acp-calc .```

Then run the image container at port 5000:

```docker run -it -p 5000:5000 p4-acp-calc```

Goto a browser you have like Chrome, then goto ```localhost:5000``` and it'll redirect you to the html.calc page.

Enjoy calculating!


## Calculation

Following the [ACP Brevet Control Times Calculator](https://rusa.org/pages/acp-brevet-control-times-calculator), open and close times are determined on a minimum and maximum speed between kilometer(s) x and kilometers (y).

|Control Location (km) | Min Speed (km/h) | Max Speed (km/h) |
|:--------------------:|:----------------:|:----------------:|
|0-200                 | 15               | 34               |
|200-400               | 15               | 32               |
|400-600               | 15               | 30               |
|600-1000              | 11.428           | 28               |
|1000-1300             | 13.333           | 26               |

__Opening times are based on max speed:__

Example:

&emsp;&emsp;&emsp;60km: (60km / 34 km/h) = 1h46

&emsp;&emsp;&emsp;250km: (200km / 34 km/h) + (50km / 32 km/h) = 7h27

__Closing times are based on min speed:__

Example:

&emsp;&emsp;&emsp;60km: (60 km / 15 km/h) = 4h00

&emsp;&emsp;&emsp;250km: (200km / 15 km/h) + (50km / 15 km/h) = 16h40

Open and closing times cap out at the brevet length--even if control point is further than brevet length.
Control points cannot exceed 120% of the brevet length.

Example for 200km brevet open time (same concept for closing times, too):

&emsp;&emsp;&emsp;(200km / 34 km/h) = 5h53

&emsp;&emsp;&emsp;(240km / 34 km/h) = 5h53

&emsp;&emsp;&emsp;(241km / 34 km/h) = Control point too far


## Files

- README.md 		&emsp;&emsp;&emsp;&emsp;&emsp;# Description of Repo
- brevets
	- acp_times.py	&emsp;&emsp;&emsp;# Backend Calculations for open and closing times
	- config.py		&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;# Configuration
	- Dockerfile	&emsp;&emsp;&emsp;&emsp;&nbsp;# Dockerfile to build container
	- flask_brevets.py	&emsp;&nbsp;&nbsp;&nbsp;# Flask for communication between front and backend
	- requirements.txt	&emsp;&nbsp;&nbsp;# Required modules needed to run program (for Dockerfile)
- templates
	- 404.html		&emsp;&emsp;&emsp;&emsp;&emsp;# 404 Page not found page
	- calc.html	&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;# Front-end interactive calculator
- tests
	- test_acp_times.py	&emsp;# Test suite for acp_times.py
- static		&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;	# Contains simple js, and css files

## Resources
- [ACP Brevet Control Times Calculator](https://rusa.org/pages/acp-brevet-control-times-calculator)
- [RUSA rules](https://rusa.org/pages/rulesForRiders)